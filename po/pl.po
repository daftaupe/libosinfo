# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Piotr Drąg <piotrdrag@gmail.com>, 2012.
# Piotr Drąg <piotrdrag@gmail.com>, 2015. #zanata
# Piotr Drąg <piotrdrag@gmail.com>, 2016. #zanata
# Piotr Drąg <piotrdrag@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-04 16:42+0000\n"
"PO-Revision-Date: 2017-03-28 10:42-0400\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish (http://www.transifex.com/projects/p/libosinfo/"
"language/pl/)\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Zanata 3.9.6\n"

#: ../osinfo/osinfo_avatar_format.c:109
msgid "The allowed mime-types for the avatar"
msgstr "Dozwolone typy MIME dla awatara"

#: ../osinfo/osinfo_avatar_format.c:123
msgid "The required width (in pixels) of the avatar"
msgstr "Wymagana szerokość (w pikselach) awatara"

#: ../osinfo/osinfo_avatar_format.c:140
msgid "The required height (in pixels) of the avatar."
msgstr "Wymagana wysokość (w pikselach) awatara."

#: ../osinfo/osinfo_avatar_format.c:157
msgid "Whether alpha channel is supported in the avatar."
msgstr "Czy kanał alfa jest obsługiwany w awatarze."

#: ../osinfo/osinfo_deployment.c:154
msgid "Operating system"
msgstr "System operacyjny"

#: ../osinfo/osinfo_deployment.c:169
msgid "Virtualization platform"
msgstr "Platforma wirtualizacji"

#: ../osinfo/osinfo_devicelink.c:129
msgid "Target device"
msgstr "Urządzenie docelowe"

#: ../osinfo/osinfo_devicelinkfilter.c:134
msgid "Device link target filter"
msgstr "Filtr celu dowiązania urządzenia"

#: ../osinfo/osinfo_entity.c:136
msgid "Unique identifier"
msgstr "Unikalny identyfikator"

#: ../osinfo/osinfo_install_config_param.c:152
msgid "Parameter name"
msgstr "Nazwa parametru"

#: ../osinfo/osinfo_install_config_param.c:168
msgid "Parameter policy"
msgstr "Polityka parametru"

#: ../osinfo/osinfo_install_config_param.c:184
msgid "Parameter Value Mapping"
msgstr "Mapowanie wartości parametru"

#: ../osinfo/osinfo_install_script.c:192
msgid "URI for install script template"
msgstr "Adres URI do szablonu skryptu instalacji"

#: ../osinfo/osinfo_install_script.c:204
msgid "Data for install script template"
msgstr "Dane dla szablonu skryptu instalacji"

#: ../osinfo/osinfo_install_script.c:216
msgid "Install script profile name"
msgstr "Nazwa profilu skryptu instalacji"

#: ../osinfo/osinfo_install_script.c:228
msgid "Product key format mask"
msgstr "Maska formatu klucza produktu"

#: ../osinfo/osinfo_install_script.c:238
msgid "Expected path format"
msgstr "Oczekiwany format ścieżki"

#: ../osinfo/osinfo_install_script.c:249
msgid "Expected avatar format"
msgstr "Oczekiwany format awatara"

#: ../osinfo/osinfo_install_script.c:585
msgid "Unable to create XML parser context"
msgstr "Nie można utworzyć kontekstu parsera XML"

#: ../osinfo/osinfo_install_script.c:593
msgid "Unable to read XSL template"
msgstr "Nie można odczytać szablonu XSL"

#: ../osinfo/osinfo_install_script.c:599
msgid "Unable to parse XSL template"
msgstr "Nie można przetworzyć szablonu XSL"

#: ../osinfo/osinfo_install_script.c:672 ../osinfo/osinfo_install_script.c:708
#, c-format
msgid "Unable to create XML node '%s': '%s'"
msgstr "Nie można utworzyć węzła XML „%s”: „%s”"

#: ../osinfo/osinfo_install_script.c:680
#, c-format
msgid "Unable to create XML node 'id': '%s'"
msgstr "Nie można utworzyć węzła XML „id”: „%s”"

#: ../osinfo/osinfo_install_script.c:686 ../osinfo/osinfo_install_script.c:714
#, c-format
msgid "Unable to add XML child '%s'"
msgstr "Nie można dodać elementu potomnego XML „%s”"

#: ../osinfo/osinfo_install_script.c:761 ../osinfo/osinfo_install_script.c:772
#: ../osinfo/osinfo_install_script.c:796
#, c-format
msgid "Unable to set XML root '%s'"
msgstr "Nie można ustawić głównego elementu XML „%s”"

#: ../osinfo/osinfo_install_script.c:784
#, c-format
msgid "Unable to set 'media' node: '%s'"
msgstr "Nie można ustawić węzła „media”: „%s”"

#: ../osinfo/osinfo_install_script.c:819
msgid "Unable to create XSL transform context"
msgstr "Nie można utworzyć kontekstu przekształcenia XSL"

#: ../osinfo/osinfo_install_script.c:824
msgid "Unable to apply XSL transform context"
msgstr "Nie można zastosować kontekstu przekształcenia XSL"

#: ../osinfo/osinfo_install_script.c:829
msgid "Unable to convert XSL output to string"
msgstr "Nie można przekonwertować wyjścia XSL na ciąg"

#: ../osinfo/osinfo_install_script.c:889
#, c-format
msgid "Failed to load script template %s: "
msgstr "Wczytanie szablonu skryptu %s się nie powiodło: "

#: ../osinfo/osinfo_install_script.c:904
#, c-format
msgid "Failed to apply script template %s: "
msgstr "Zastosowanie szablonu skryptu %s się nie powiodło: "

#: ../osinfo/osinfo_install_script.c:958 ../osinfo/osinfo_install_script.c:1600
#: ../osinfo/osinfo_install_script.c:1648
msgid "Failed to apply script template: "
msgstr "Zastosowanie szablonu skryptu się nie powiodło: "

#: ../osinfo/osinfo_list.c:132
msgid "List element type"
msgstr "Typ elementu listy"

#: ../osinfo/osinfo_loader.c:195
#, c-format
msgid "Expected a nodeset in XPath query %s"
msgstr "Oczekiwano nodeset w zapytaniu XPath %s"

#: ../osinfo/osinfo_loader.c:275 ../osinfo/osinfo_loader.c:420
msgid "Expected a text node attribute value"
msgstr "Oczekiwano wartości atrybutu węzła tekstu"

#: ../osinfo/osinfo_loader.c:574
msgid "Missing device id property"
msgstr "Brak właściwości identyfikatora urządzenia"

#: ../osinfo/osinfo_loader.c:609
msgid "Missing device link id property"
msgstr "Brak właściwości identyfikatora dowiązania urządzenia"

#: ../osinfo/osinfo_loader.c:659
msgid "Missing product upgrades id property"
msgstr "Brak właściwości identyfikatora aktualizacji produktu"

#: ../osinfo/osinfo_loader.c:732
msgid "Missing platform id property"
msgstr "Brak właściwości identyfikatora platformy"

#: ../osinfo/osinfo_loader.c:766
msgid "Missing deployment id property"
msgstr "Brak właściwości identyfikatora "

#: ../osinfo/osinfo_loader.c:776
msgid "Missing deployment os id property"
msgstr "Brak właściwości identyfikatora wdrożenia"

#: ../osinfo/osinfo_loader.c:786
msgid "Missing deployment platform id property"
msgstr "Brak właściwości identyfikatora platformy wdrożenia"

#: ../osinfo/osinfo_loader.c:825 ../osinfo/osinfo_loader.c:1411
msgid "Missing os id property"
msgstr "Brak właściwości identyfikatora systemu operacyjnego"

#: ../osinfo/osinfo_loader.c:949
msgid "Missing install script id property"
msgstr "Brak właściwości identyfikatora skryptu instalacji"

#: ../osinfo/osinfo_loader.c:1526
msgid "Missing OS install script property"
msgstr "Brak właściwości skryptu instalacji systemu operacyjnego"

#: ../osinfo/osinfo_loader.c:1590
msgid "Incorrect root element"
msgstr "Niepoprawny główny element"

#: ../osinfo/osinfo_loader.c:1657
msgid "Unable to construct parser context"
msgstr "Nie można skonstruować kontekstu parsera"

#: ../osinfo/osinfo_loader.c:1679
msgid "Missing root XML element"
msgstr "Brak głównego elementu XML"

#: ../osinfo/osinfo_loader.c:2294
#, c-format
msgid ""
"$OSINFO_DATA_DIR is deprecated, please use $OSINFO_SYSTEM_DIR instead. "
"Support for $OSINFO_DATA_DIR will be removed in a future release\n"
msgstr ""

#: ../osinfo/osinfo_loader.c:2330 ../osinfo/osinfo_loader.c:2362
#, c-format
msgid ""
"%s is deprecated, please use %s instead. Support for %s will be removed in a "
"future release\n"
msgstr ""
"%s jest przestarzałe, należy używać %s zamiast tego. Obsługa %s zostanie "
"usunięta w przyszłym wydaniu\n"

#: ../osinfo/osinfo_media.c:391 ../osinfo/osinfo_resources.c:170
#: ../osinfo/osinfo_tree.c:273
msgid "CPU Architecture"
msgstr "Architektura procesora"

#: ../osinfo/osinfo_media.c:404
msgid "The URL to this media"
msgstr "Adres URL do tego nośnika"

#: ../osinfo/osinfo_media.c:417 ../osinfo/osinfo_tree.c:299
msgid "The expected ISO9660 volume ID"
msgstr "Oczekiwany identyfikator woluminu ISO9660"

#: ../osinfo/osinfo_media.c:430 ../osinfo/osinfo_tree.c:312
msgid "The expected ISO9660 publisher ID"
msgstr "Oczekiwany identyfikator wydawcy ISO9660"

#: ../osinfo/osinfo_media.c:443 ../osinfo/osinfo_tree.c:325
msgid "The expected ISO9660 application ID"
msgstr "Oczekiwany identyfikator aplikacji ISO9660"

#: ../osinfo/osinfo_media.c:456 ../osinfo/osinfo_tree.c:338
msgid "The expected ISO9660 system ID"
msgstr "Oczekiwany identyfikator systemu ISO9660"

#: ../osinfo/osinfo_media.c:469 ../osinfo/osinfo_tree.c:351
msgid "The path to the kernel image"
msgstr "Ścieżka do obrazu jądra"

#: ../osinfo/osinfo_media.c:482
msgid "The path to the initrd image"
msgstr "Ścieżka do obrazy initrd"

#: ../osinfo/osinfo_media.c:495
msgid "Media provides an installer"
msgstr "Nośnik dostarcza instalator"

#: ../osinfo/osinfo_media.c:508
msgid "Media can boot directly w/o installation"
msgstr "Nośnik może być uruchamiany bezpośrednio bez instalacji"

#: ../osinfo/osinfo_media.c:530
msgid "Number of installer reboots"
msgstr "Liczba ponownych uruchomień instalatora"

#: ../osinfo/osinfo_media.c:548
msgid "Information about the operating system on this media"
msgstr "Informacje o systemie operacyjnym na tym nośniku"

#: ../osinfo/osinfo_media.c:570
msgid "Supported languages"
msgstr "Obsługiwane języki"

#: ../osinfo/osinfo_media.c:582
msgid "Expected ISO9660 volume size, in bytes"
msgstr "Oczekiwany rozmiar woluminu ISO9660 w bajtach"

#: ../osinfo/osinfo_media.c:602
msgid "Whether the media should be ejected after the installtion process"
msgstr ""

#: ../osinfo/osinfo_media.c:719
msgid "Failed to read supplementary volume descriptor: "
msgstr "Odczytanie dodatkowego deskryptora woluminu się nie powiodło: "

#: ../osinfo/osinfo_media.c:726
#, c-format
msgid "Supplementary volume descriptor was truncated"
msgstr "Dodatkowy deskryptor woluminu został skrócony"

#: ../osinfo/osinfo_media.c:750
#, c-format
msgid "Install media is not bootable"
msgstr "Nośnik instalacji nie jest startowy"

#: ../osinfo/osinfo_media.c:812
msgid "Failed to read primary volume descriptor: "
msgstr "Odczytanie głównego deskryptora woluminu się nie powiodło: "

#: ../osinfo/osinfo_media.c:819
#, c-format
msgid "Primary volume descriptor was truncated"
msgstr "Główny deskryptor woluminu został skrócony"

#: ../osinfo/osinfo_media.c:851
#, c-format
msgid "Insufficient metadata on installation media"
msgstr "Niewystarczające metadane na nośniku instalacji"

#: ../osinfo/osinfo_media.c:885
#, c-format
msgid "Failed to skip %d bytes"
msgstr "Pominięcie %d B się nie powiodło"

#: ../osinfo/osinfo_media.c:890
#, c-format
msgid "No volume descriptors"
msgstr "Brak deskryptorów woluminu"

#: ../osinfo/osinfo_media.c:921
msgid "Failed to open file"
msgstr "Otwarcie pliku się nie powiodło"

#: ../osinfo/osinfo_os.c:151
msgid "Generic Family"
msgstr "Ogólna rodzina"

#: ../osinfo/osinfo_os.c:167
msgid "Generic Distro"
msgstr "Ogólna dystrybucja"

#: ../osinfo/osinfo_product.c:169 ../tools/osinfo-query.c:59
#: ../tools/osinfo-query.c:83 ../tools/osinfo-query.c:109
msgid "Name"
msgstr "Nazwa"

#: ../osinfo/osinfo_product.c:182 ../tools/osinfo-query.c:57
#: ../tools/osinfo-query.c:81
msgid "Short ID"
msgstr "Krótki identyfikator"

#: ../osinfo/osinfo_product.c:195 ../tools/osinfo-query.c:67
#: ../tools/osinfo-query.c:87 ../tools/osinfo-query.c:101
msgid "Vendor"
msgstr "Producent"

#: ../osinfo/osinfo_product.c:208 ../tools/osinfo-query.c:61
#: ../tools/osinfo-query.c:85
msgid "Version"
msgstr "Wersja"

#: ../osinfo/osinfo_product.c:221
msgid "Codename"
msgstr "Nazwa kodowa"

#: ../osinfo/osinfo_product.c:234
msgid "URI of the logo"
msgstr "Adres URI do logo"

#: ../osinfo/osinfo_resources.c:186
msgid "CPU frequency in hertz (Hz)"
msgstr "Częstotliwość procesora w hercach (Hz)"

#: ../osinfo/osinfo_resources.c:203
msgid "Number of CPUs"
msgstr "Liczba procesorów"

#: ../osinfo/osinfo_resources.c:220
msgid "Amount of Random Access Memory (RAM) in bytes"
msgstr "Ilość pamięci swobodnego dostępu (RAM) w bajtach"

#: ../osinfo/osinfo_resources.c:237
msgid "Amount of storage space in bytes"
msgstr "Ilość przestrzeni pamięci masowej w bajtach"

#: ../osinfo/osinfo_tree.c:286
msgid "The URL to this tree"
msgstr "Adres URL do tego drzewa"

#: ../osinfo/osinfo_tree.c:364
msgid "The path to the inirtd image"
msgstr "Ścieżka do obrazu initrd"

#: ../osinfo/osinfo_tree.c:377
msgid "The path to the bootable ISO image"
msgstr "Ścieżka do startowego obrazu ISO"

#: ../osinfo/osinfo_tree.c:601
msgid "Failed to load .treeinfo file: "
msgstr "Wczytanie pliku .treeinfo się nie powiodło: "

#: ../osinfo/osinfo_tree.c:611
msgid "Failed to process keyinfo file: "
msgstr "Przetworzenie pliku keyinfo się nie powiodło: "

#: ../osinfo/osinfo_os_variant.c:115
msgid "The name to this variant"
msgstr "Nazwa tego wariantu"

#: ../tools/osinfo-detect.c:65 ../tools/osinfo-detect.c:86
#, c-format
msgid "Invalid value '%s'"
msgstr "Nieprawidłowa wartość „%s”"

#: ../tools/osinfo-detect.c:98
msgid "Output format. Default: plain"
msgstr "Format wyjściowy. Domyślny: plain"

#: ../tools/osinfo-detect.c:99
msgid "plain|env."
msgstr "plain|env."

#: ../tools/osinfo-detect.c:102
msgid "URL type. Default: media"
msgstr "Typ adresu URL. Domyślny: media"

#: ../tools/osinfo-detect.c:103
msgid "media|tree."
msgstr "media|tree."

#: ../tools/osinfo-detect.c:113
#, c-format
msgid "Media is bootable.\n"
msgstr "Nośnik jest startowy.\n"

#: ../tools/osinfo-detect.c:118
#, c-format
msgid "Media is not bootable.\n"
msgstr "Nośnik nie jest startowy.\n"

#: ../tools/osinfo-detect.c:155
#, c-format
msgid "Media is an installer for OS '%s'\n"
msgstr "Nośnik jest instalatorem dla systemu operacyjnego „%s”\n"

#: ../tools/osinfo-detect.c:157
#, c-format
msgid "Media is live media for OS '%s'\n"
msgstr "Nośnik jest nośnikiem live dla systemu operacyjnego „%s”\n"

#: ../tools/osinfo-detect.c:162
#, c-format
msgid "Available OS variants on media:\n"
msgstr "Dostępne warianty systemu operacyjnego na tym nośniku:\n"

#: ../tools/osinfo-detect.c:207
#, c-format
msgid "Tree is an installer for OS '%s'\n"
msgstr "Drzewo jest instalatorem dla systemu operacyjnego „%s”\n"

#: ../tools/osinfo-detect.c:224
msgid "- Detect if media is bootable and the relevant OS and distribution."
msgstr ""
"— wykrywa odpowiedni system operacyjny i dystrybucję oraz czy nośnik jest "
"startowy."

#: ../tools/osinfo-detect.c:228 ../tools/osinfo-query.c:415
#, c-format
msgid "Error while parsing commandline options: %s\n"
msgstr "Błąd podczas przetwarzania opcji wiersza poleceń: %s\n"

#: ../tools/osinfo-detect.c:246 ../tools/osinfo-install-script.c:342
#: ../tools/osinfo-query.c:431
#, c-format
msgid "Error loading OS data: %s\n"
msgstr "Błąd podczas wczytywania danych systemu operacyjnego: %s\n"

#: ../tools/osinfo-detect.c:262
#, c-format
msgid "Error parsing media: %s\n"
msgstr "Błąd podczas przetwarzania nośnika: %s\n"

#: ../tools/osinfo-detect.c:280
#, c-format
msgid "Error parsing installer tree: %s\n"
msgstr "Błąd podczas przetwarzania drzewa instalatora: %s\n"

#: ../tools/osinfo-install-script.c:53
#, c-format
msgid "Expected configuration key=value"
msgstr "Oczekiwano klucz=wartość konfiguracji"

#: ../tools/osinfo-install-script.c:71
msgid "Install script profile"
msgstr "Profil skryptu instalacji"

#: ../tools/osinfo-install-script.c:73
msgid "Install script output directory"
msgstr "Katalog wyjściowy skryptu instalacji"

#: ../tools/osinfo-install-script.c:75
msgid "The output filename prefix"
msgstr "Przedrostek nazwy pliku wyjściowego"

#: ../tools/osinfo-install-script.c:78
msgid "Set configuration parameter"
msgstr "Ustawia parametr konfiguracji"

#: ../tools/osinfo-install-script.c:80
msgid "List configuration parameters"
msgstr "Wyświetla listę parametrów konfiguracji"

#: ../tools/osinfo-install-script.c:82
msgid "List install script profiles"
msgstr "Wyświetla listę profili skryptów instalacyjnych"

#: ../tools/osinfo-install-script.c:84
msgid "List supported injection methods"
msgstr "Wyświetla listę obsługiwanych metod wprowadzania"

#: ../tools/osinfo-install-script.c:86
msgid "Do not display output filenames"
msgstr "Bez wyświetlania wyjściowych nazw plików"

#: ../tools/osinfo-install-script.c:143
#, c-format
msgid "No install script for profile '%s' and OS '%s'"
msgstr ""
"Brak skryptu instalacyjnego dla profilu „%s” i systemu operacyjnego „%s”"

#: ../tools/osinfo-install-script.c:159
msgid "required"
msgstr "wymagane"

#: ../tools/osinfo-install-script.c:159
msgid "optional"
msgstr "opcjonalne"

#: ../tools/osinfo-install-script.c:245
#, c-format
msgid "No install script for profile '%s' and OS '%s'\n"
msgstr ""
"Brak skryptu instalacyjnego dla profilu „%s” i systemu operacyjnego „%s”\n"

#: ../tools/osinfo-install-script.c:274
#, c-format
msgid "Unable to generate install script: %s\n"
msgstr "Nie można utworzyć skryptu instalacji: %s\n"

#: ../tools/osinfo-install-script.c:311
msgid "- Generate an OS install script"
msgstr "— tworzy skrypt instalacji systemu operacyjnego"

#: ../tools/osinfo-install-script.c:314
#, c-format
msgid "Error while parsing options: %s\n"
msgstr "Błąd podczas przetwarzania opcji: %s\n"

#: ../tools/osinfo-install-script.c:332
msgid ""
"Only one of --list-profile, --list-config and --list-injection-methods can "
"be requested"
msgstr ""
"Tylko jeden parametr z --list-profile, --list-config i --list-injection-"
"methods może zostać zażądany"

#: ../tools/osinfo-install-script.c:361
#, c-format
msgid "Error finding OS: %s\n"
msgstr "Błąd podczas wyszukiwania systemu operacyjnego: %s\n"

#: ../tools/osinfo-query.c:63
msgid "Family"
msgstr "Rodzina"

#: ../tools/osinfo-query.c:65
msgid "Distro"
msgstr "Dystrybucja"

#: ../tools/osinfo-query.c:69 ../tools/osinfo-query.c:89
msgid "Release date"
msgstr "Data wydania"

#: ../tools/osinfo-query.c:71 ../tools/osinfo-query.c:91
msgid "End of life"
msgstr "Koniec cyklu wydawniczego"

#: ../tools/osinfo-query.c:73 ../tools/osinfo-query.c:93
msgid "Code name"
msgstr "Nazwa kodowa"

#: ../tools/osinfo-query.c:75 ../tools/osinfo-query.c:95
#: ../tools/osinfo-query.c:115 ../tools/osinfo-query.c:121
msgid "ID"
msgstr "Identyfikator"

#: ../tools/osinfo-query.c:103
msgid "Vendor ID"
msgstr "Identyfikator producenta"

#: ../tools/osinfo-query.c:105
msgid "Product"
msgstr "Produkt"

#: ../tools/osinfo-query.c:107
msgid "Product ID"
msgstr "Identyfikator produktu"

#: ../tools/osinfo-query.c:111
msgid "Class"
msgstr "Klasa"

#: ../tools/osinfo-query.c:113
msgid "Bus"
msgstr "Magistrala"

#: ../tools/osinfo-query.c:153 ../tools/osinfo-query.c:191
#, c-format
msgid "Unknown property name %s"
msgstr "Nieznana nazwa właściwości %s"

#: ../tools/osinfo-query.c:177
msgid "Syntax error in condition, expecting KEY=VALUE"
msgstr "Błąd składni w warunku, oczekiwanie KLUCZ=WARTOŚĆ"

#: ../tools/osinfo-query.c:403
msgid "Sort column"
msgstr "Porządkowanie kolumn"

#: ../tools/osinfo-query.c:405
msgid "Display fields"
msgstr "Wyświetlanie pól"

#: ../tools/osinfo-query.c:410
msgid "- Query the OS info database"
msgstr "— odpytuje bazę danych informacji o systemach operacyjnych"

#: ../tools/osinfo-query.c:422
#, c-format
msgid "Missing data type parameter\n"
msgstr "Brak parametru typu danych\n"

#: ../tools/osinfo-query.c:450
#, c-format
msgid "Unknown type '%s' requested\n"
msgstr "Zażądano nieznanego typu „%s”\n"

#: ../tools/osinfo-query.c:455
#, c-format
msgid "Unable to construct filter: %s\n"
msgstr "Nie można skonstruować filtru: %s\n"

#: ../tools/osinfo-query.c:460
#, c-format
msgid "Unable to set field visibility: %s\n"
msgstr "Nie można ustawić widoczności pola: %s\n"
